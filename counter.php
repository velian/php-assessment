<!DOCTYPE html>
<html>
<body>

<?php
	for ($i = 1; $i < 100; $i ++)
    {
        if ( $i % 3 == 0)
        {           
            if ($i % 5 == 0)
            {
                echo "Triple-Fiver at " . $i . ", ";
                continue;
            }
            
            echo "Triple at " . $i . ", ";
            continue;
        }
        
        if ( $i % 5 == 0)
        {
            echo "Fiver at " . $i . ", ";
        }
    }
?>

</body>
</html>